# Programación
A continuación se enlistan los programas del curso:

 * Suma: [código](https://gitlab.com/ESCOM/programas-propedeutico/blob/master/suma.py) - [documentación](https://gitlab.com/ESCOM/programas-propedeutico/wikis/suma)
 * Ecuación cuadrática
 * Factorial
 * Inverso Factorial
 * Fibonacci
 * Parseval
 * Serie de Taylor
 * Palindomo
 * Editor de texto
 * 8 reinas
 * Transformada de wavelet (Matriz)
 * Transformada de wavelet (imagen)