if(process.argv.length != 3) {
  console.log("Forma de uso: node factorial.js [numero]")
} else {
    const n = parseInt(process.argv[2])
    let total = 1
    for(cont = 1; cont <= n; cont++) {
        total *= cont
    }
    console.log(total)
}