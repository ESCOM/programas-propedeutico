if(process.argv.length != 3) {
    console.log("Forma de uso: node taylor.js [x]")
} else {
  const x = parseInt(process.argv[2])
  let anterior = 0
  let actual = 1
  let iteraciones = 1
  while(Math.abs(actual - anterior) > 0.0001) {
      anterior = actual
      let auxiliar = 0
      for(let contador = 0; contador < iteraciones; contador++) {
          factorial = 1
          for(let cont = 1; cont < 2 * contador + 1; cont++) {
              factorial = factorial * cont
          }
          auxiliar = auxiliar + (Math.pow(-1, contador) / factorial) * Math.pow(x, 2 * contador + 1)
      }
      actual = auxiliar
      iteraciones = iteraciones + 1
  }
  console.log("sen(" + x + ") = " + actual)
  console.log("Iteraciones: " + iteraciones)
}
