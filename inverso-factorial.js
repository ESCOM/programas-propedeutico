if(process.argv.length != 3) {
  console.log("Forma de uso: node inverso-factorial.js [numero]")
} else {
    const n = parseInt(process.argv[2])
    let factorial = 1
    for(cont = 1; cont <= n; cont++) {
        factorial *= cont
    }
    const total = 1 / factorial
    console.log(total)
}