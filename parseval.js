if(process.argv.length != 2) {
    console.log("Forma de uso: node parseval.js")
} else {
    let anterior = 0
    let actual = 0
    let n = 1
    while(Math.abs(actual - anterior) > 0.0001) {
        anterior = actual
        actual = actual + 1 / Math.pow(n, 4)
        n = n + 1
    }
    console.log(n)
}