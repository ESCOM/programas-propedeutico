if(process.argv.length != 2) {
    console.log("Forma de uso: node reinas.js")
} else {
    let reinas = ["R1", "R2", "R3", "R4", "R5", "R6", "R7", "R8"]
    for (let casilla = 0; casilla < 8; casilla++) {
        let tablero = [
            ["  ", "  ", "  ", "  ", "  ", "  ", "  ", "  "],
            ["  ", "  ", "  ", "  ", "  ", "  ", "  ", "  "],
            ["  ", "  ", "  ", "  ", "  ", "  ", "  ", "  "],
            ["  ", "  ", "  ", "  ", "  ", "  ", "  ", "  "],
            ["  ", "  ", "  ", "  ", "  ", "  ", "  ", "  "],
            ["  ", "  ", "  ", "  ", "  ", "  ", "  ", "  "],
            ["  ", "  ", "  ", "  ", "  ", "  ", "  ", "  "],
            ["  ", "  ", "  ", "  ", "  ", "  ", "  ", "  "]
        ]
        tablero[0][casilla] = reinas[0]
        let fila = 1
        let columna_r = 0
        let se_pudo = true
        for (let reina = 1; reina < 8; reina++) {
            let columna = columna_r
            let comio = false
            do {
                let queda_reina = false
                while (columna < 8 && queda_reina === false) {
                    if (tablero[fila][columna] === "  ") {
                        tablero[fila][columna] = reinas[reina]
                        queda_reina = true
                    } else {
                        tablero[fila][columna] = "  "
                        columna = columna + 1
                    }
                }
                let validar_fila = 0
                comio = false
                if (columna < 8) {
                    do {
                        validar_columna_1 = columna - (fila - validar_fila)
                        validar_columna_2 = columna + (fila - validar_fila)
                        if (tablero[validar_fila][columna] !== "  ") {
                            comio = true
                            tablero[fila][columna] = "  "
                        } else if (validar_columna_1 >= 0 && tablero[validar_fila][validar_columna_1] !== "  ") {
                            comio = true
                            tablero[fila][columna] = "  "
                        } else if (validar_columna_2 < 8 && tablero[validar_fila][validar_columna_2] !== "  ") {
                            comio = true
                            tablero[fila][columna] = "  "
                        }
                        validar_fila = validar_fila + 1
                    } while (validar_fila < fila && columna < 8 && comio === false);
                }
                if (comio === true) {
                    columna = columna + 1
                }
            } while (comio === true && columna < 8);
            if (columna > 7) {
                fila = fila - 1
                reina = reina - 2
            } else {
                fila = fila + 1
            }
        }
        console.log(tablero);
        console.log("----------------------------------------------------------------------------------");
    }
}