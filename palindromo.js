if(process.argv.length != 3) {
    console.log("Forma de uso: node palindromo.js [\"frase\"]")
} else {
    let s = process.argv[2]
    for(let p = 0; p < s.length ; p++) {
        c = s[p]
        if(c === " ") {
            let s1 = ""
            for(let i = 0; i < p; i ++) {
                s1 = s1 + s[i]
            }
            let s2 = ""
            for(let i = p + 1; i < s.length; i ++) {
                s2 = s2 + s[i]
            }
            s = s1 + s2
        }
    }
    t = s.length -1
    b = true
    p = 0
    while(p < t && b === true) {
      if(s[p] != s[t]) {
          b = false
      } else {
          p = p + 1
          t = t - 1
      }
    }
    console.log(b ? "Es palindrome": "No es palindrome")
}