if(process.argv.length != 4) {
  console.log("Forma de uso: node suma.js [primer_sumando] [segundo_sumando]")
} else {
  const x = parseInt(process.argv[2])
  const y = parseInt(process.argv[3])
  const z = x + y
  console.log(z)
}