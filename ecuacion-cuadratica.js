if(process.argv.length != 5) {
  console.log("Forma de uso: node ecuacion-cuadratica.js [cuadrado] [lineal] [constante]")
} else {
    const a = parseInt(process.argv[2])
    const b = parseInt(process.argv[3])
    const c = parseInt(process.argv[4])
    const x1 = (Math.sqrt(Math.pow(b, 2) - 4 * a * c) - b) / (2 * a)
    const x2 = (- Math.sqrt(Math.pow(b, 2) - 4 * a * c) - b) / (2 * a)
    console.log("x1 = " + x1)
    console.log("x2 = " + x2)
}